const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const dotenv = require("dotenv");
const result = dotenv.config({ debug: true });

const config = require("./config/config");

const app = express();

app.use(express.static("public"));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Configuring the database
const dbConfig = require("./config/database.config.js");
const mongoose = require("mongoose");

mongoose.Promise = global.Promise;

const db = require("./app/models");
const Role = db.role;

// Connecting to the database
db.mongoose
  .connect(dbConfig.url, {
    useCreateIndex: true,
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Successfully connected to the database");
    initial();
  })
  .catch((err) => {
    console.log("Could not connect to the database. Exiting now...", err);
    process.exit();
  });

function initial() {
  Role.estimatedDocumentCount((err, count) => {
    if (!err && count === 0) {
      new Role({
        name: "user",
      }).save((err) => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'user' to roles collection");
      });

      new Role({
        name: "moderator",
      }).save((err) => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'moderator' to roles collection");
      });

      new Role({
        name: "admin",
      }).save((err) => {
        if (err) {
          console.log("error", err);
        }

        console.log("added 'admin' to roles collection");
      });
    }
  });
}

app.get("/", (req, res) => {
  const help = `
    <pre>
      Welcome to the AvonHealth API!
      Use an Authorization header to work with your own data:
      fetch(url, { headers: { 'Authorization': 'whatever-you-want' }})
      The following endpoints are available:
      GET /users
      DELETE /users/:id
      users /user { text }
    </pre>
  `;

  res.send(help);
});

/*
app.use((req, res, next) => {
  const token = req.get("Authorization");

  if (token) {
    req.token = token;
    next();
  } else {
    res.status(403).send({
      error:
        "Please provide an Authorization header to identify yourself (can be whatever you want)",
    });
  }
});
*/
app.listen(config.port, () => {
  console.log("Server listening on port %s, Ctrl+C to stop", config.port);
});

require("./app/routes/auth.routes")(app);
require("./app/routes/user.routes")(app);
const routes = require("./app/routes/Routes"); //importing route
routes(app); //register the route
