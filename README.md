## AvonHealth API

## Stack

- Node + Express
- MongoDb

## Localhost

Setup:

1. `yarn or npm install`
2. Make sure your mongodb is running on local `mongod`
3. `yarn dev` for development `yarn start` for production
4. copy `.env.sample` into `.env` and fill in credentials

To run:

1. `yarn dev` or `npm run dev`
2. API end point would be `http://localhost:5001`
