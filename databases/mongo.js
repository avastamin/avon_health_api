const { MongoMemoryServer } = require("mongodb-memory-server");
const { MongoClient } = require("mongodb");
// https://auth0.com/blog/node-js-and-express-tutorial-building-and-securing-restful-apis/

let database = null;

async function startDatabase() {
  const mongo = new MongoMemoryServer();
  const mongoDBURL = await mongo.getConnectionString();
  console.log("mongoDBURL:", mongoDBURL);

  const connection = await MongoClient.connect(mongoDBURL, {
    useNewUrlParser: true,
  });
  database = connection.db();
}

async function getDatabase() {
  if (!database) await startDatabase();
  return database;
}

module.exports = {
  getDatabase,
  startDatabase,
};
