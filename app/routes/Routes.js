"use strict";
module.exports = function (app) {
  const user = require("../controllers/user.Controller.js");

  // Create a new User
  app.post("/users", user.create);
  //app.post("/users/login", user.login);

  // Retrieve all User
  app.get("/users", user.findAll);

  // Retrieve a single User with userId
  app.get("/users/:userId", user.findOne);

  // Update a User with userId
  app.put("/users/:userId", user.update);

  // Delete a User with userId
  app.delete("/users/:userId", user.delete);
};
