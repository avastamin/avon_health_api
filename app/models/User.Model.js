const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcrypt");
const SALT_WORK_FACTOR = 10;

const UserSchema = new Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
      index: { unique: true },
      validate: {
        isAsync: true,
        validator: function (value, isValid) {
          const self = this;
          return self.constructor
            .findOne({ email: value })
            .exec(function (err, user) {
              if (err) {
                throw err;
              } else if (user) {
                if (self.id === user.id) {
                  // if finding and saving then it's valid even for existing email
                  return isValid(true);
                }
                return isValid(false);
              } else {
                return isValid(true);
              }
            });
        },
        message: "The email address is already taken!",
      },
    },
    password: { type: String, required: true },
    gender: { type: String, required: true },
    roles: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Role",
      },
    ],
    year_born: { type: Number },
    blood_pressure_systolic: { type: Number },
    blood_pressure_diastolic: { type: Number },
    height: { type: Number } /*total inches*/,
    waist: { type: Number } /*inches*/,
    hip: { type: Number } /*inches*/,
    firstname: { type: String },
    signin_dt: { type: Date },
    /*Conditions*/
    Prediabetes: { type: Boolean, default: false },
    Type_2_Diabetes: { type: Boolean, default: false },
    Hypoglycemia: { type: Boolean, default: true },
    NAFLD: { type: Boolean, default: false },
    Hypothyroidism: { type: Boolean, default: false },
    Hashimotos: { type: Boolean, default: false },
    Hypertension: { type: Boolean, default: true },
    Hypotension: { type: Boolean, default: false },
    PCOS: { type: Boolean, default: false },
    IBD: { type: Boolean, default: false },
    Crohns: { type: Boolean, default: false },
    Ulcerative_Colitis: { type: Boolean, default: false },
    C_Diff: { type: Boolean, default: false },
    Gastritis: { type: Boolean, default: false },
    Peptic_Ulcer_Disease: { type: Boolean, default: false },
    IBS: { type: Boolean, default: true },
    GERD: { type: Boolean, default: false },
    Rheumatoid_Arthritis: { type: Boolean, default: false },
    Lupus: { type: Boolean, default: false },
    Multiple_sclerosis: { type: Boolean, default: false },
    Psoriasis: { type: Boolean, default: false },
    Asthma: { type: Boolean, default: true },
    Eczema: { type: Boolean, default: false },
    Dermatitis: { type: Boolean, default: false },
    Arthritis: { type: Boolean, default: false },
    Osteopenia: { type: Boolean, default: false },
    Osteopetrosis: { type: Boolean, default: false },
    Osteoporosis: { type: Boolean, default: true },
    Fibromyalgia: { type: Boolean, default: false },
    /*Symptoms*/
    Fatigue: { type: Boolean, default: false },
    Tired_after_eating: { type: Boolean, default: false },
    Cant_lose_weight: { type: Boolean, default: false },
    Cold_Hands: { type: Boolean, default: false },
    Cold_Feet: { type: Boolean, default: false },
    Hard_see_night: { type: Boolean, default: false },
    Frequent_colds: { type: Boolean, default: false },
    Headaches: { type: Boolean, default: false },
    Joint_Pain: { type: Boolean, default: false },
    Infertility: { type: Boolean, default: false },
    Muscle_spasms: { type: Boolean, default: false },
    Skin_itch: { type: Boolean, default: false },
    Skin_acne: { type: Boolean, default: false },
    Skin_rash: { type: Boolean, default: false },
    Heartburn: { type: Boolean, default: false },
    Burping: { type: Boolean, default: false },
    Excessive_gas: { type: Boolean, default: false },
    Indigestion: { type: Boolean, default: false },
    Bloating: { type: Boolean, default: false },
    Stool_odor: { type: Boolean, default: false },
    Undigested_food: { type: Boolean, default: false },
    Diarrhea: { type: Boolean, default: false },
    Constipation: { type: Boolean, default: false },
    Depression: { type: Boolean, default: false },
    Anxiety: { type: Boolean, default: false },
    ADD: { type: Boolean, default: false },
    Autism: { type: Boolean, default: false },
    Memory_Problems: { type: Boolean, default: false },
    Difficulty_Falling_Asleep: { type: Boolean, default: false },
    Early_Waking: { type: Boolean, default: false },
    Night_Waking: { type: Boolean, default: false },
    Recent_cavities: { type: Boolean, default: false },
    Gums_bleed: { type: Boolean, default: false },
  },
  {
    timestamps: true,
  }
);

UserSchema.path("email").validate(function (email) {
  var emailRegex12321 = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

  return emailRegex12321.test(email); // Assuming email has a text attribute
}, "The e-mail field cannot be empty and should be valid email address.");

/*
UserSchema.pre("save", function (next) {
  const user = this;
  // only hash the password if it has been modified (or is new)
  if (!user.isModified("password")) return next();

  // generate a salt
  bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
    if (err) return next(err);

    // hash the password using our new salt
    bcrypt.hash(user.password, salt, function (err, hash) {
      if (err) return next(err);
      // override the cleartext password with the hashed one
      user.password = hash;
      next();
    });
  });
});
*/
UserSchema.methods.comparePassword = function (candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
    if (err) return cb(err);
    cb(null, isMatch);
  });
};

module.exports = mongoose.model("User", UserSchema);
